package com.example.zamo10.repository;

import com.example.zamo10.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Long> {
    Optional<Book> findOneById(Long id);
}
